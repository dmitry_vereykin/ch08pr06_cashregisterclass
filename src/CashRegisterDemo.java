/**
 * Created by Dmitry on 7/13/2015.
 */
import java.util.Scanner;
import java.text.DecimalFormat;

public class CashRegisterDemo {
    public static void main(String[] args) {
        RetailItem item;
        CashRegister cashRegister;
        int unitsSold;
        int choice;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("What would you like to purchase?");
        System.out.println("--------------------------------------------");
        System.out.println("Item#1........Jacket...........................$59.95");
        System.out.println("Item#2........Designer Jeans............$34.95");
        System.out.println("Item#3........Shirt..............................$24.95");

        while (true) {
            choice = keyboard.nextInt();

            if (choice == 1) {
                item = new RetailItem("Jacket", 12, 59.95);
                break;
            }
            else if (choice == 2) {
                item = new RetailItem("Designer Jeans", 40, 34.95);
                break;
            }
            else if (choice == 3) {
                item = new RetailItem("Shirt", 20, 24.95);
                break;
            }
            else {
                System.out.print("This item is not available.");
                continue;
            }
        }

        System.out.print("How many items: ");

        while (true) {
            int unitsSoldInput = keyboard.nextInt();
            if (unitsSoldInput <= item.getUnitsOnHand()) {
                unitsSold = unitsSoldInput;
                break;
            } else {
                System.out.println("Not enough items. Only " + item.getUnitsOnHand() + " in stock.");
            }
        }
        cashRegister = new CashRegister(item, unitsSold);
        DecimalFormat dollarFormat = new DecimalFormat("#,###.00");

        System.out.println("--------------------------------------------");
        System.out.println("Subtotal: $" + dollarFormat.format(cashRegister.getSubtotal()));
        System.out.println("Sales tax: $" + dollarFormat.format(cashRegister.getTax()));
        System.out.println("Total: $" + dollarFormat.format(cashRegister.getTotal()));
    }
}
